const ejs = require('ejs');
const express = require('express');
const app = express();
const port = 3000;

const indeed = require("./lib/indeed-scraper.js");
const monster = require("./lib/monster-scraper.js");
const linkedin = require("./lib/linkedin-scraper.js");
const meteojob = require("./lib/meteojob-scraper.js");
const glassdoor = require("./lib/glassdoor-scraper.js");
const iquesta = require("./lib/iquesta-scraper.js");
const lesjeudis = require("./lib/lesjeudis-scraper.js");

app.use(express.static('public'))

app.get('/', (req,res) => {
    let queryOptions = {
        query: req.query.q,
        city: req.query.loc,
        country: 'fr',
        radius: '25',
        level: '',
        jobType: 'internship',
        maxAge: '14',
        sort: 'date',
        limit: req.query.nb
    };

    let getJobs = function(website) {
        website.query(queryOptions).then(jobsRes => {
            res.render('index.ejs',
            {
                jobs: jobsRes,
                search: {
                    query: req.query.q,
                    loc: req.query.loc,
                    nb: req.query.nb,
                    site: req.query.site
                }
            });
        });
    }

    if (req.query.q !== undefined) {
        switch (req.query.site) {
            case 'indeed':
                getJobs(indeed);
                break;
            case 'monster':
                getJobs(monster);
                break;
            case 'glassdoor':
                if (queryOptions.city !== undefined && queryOptions.city !== '') {
                    glassdoor.queryLoc(queryOptions.city)
                    .then(locJSON => {
                        queryOptions['locationType'] = locJSON[0]['locationType'];
                        queryOptions['locationId'] = locJSON[0]['locationId'];
                    })
                    .then(function () {
                        getJobs(glassdoor);
                    });
                }
                else {
                    getJobs(glassdoor);
                }
                break;
            case 'lesjeudis':
                getJobs(lesjeudis);
                break;
            case 'LinkedIn':
                getJobs(linkedin);
                break;
            case 'MeteoJob':
                getJobs(meteojob);
                break;
            case 'iquesta-conseil':
                queryOptions.query = 'conseil'
                getJobs(iquesta);
                break;
            case 'iquesta-dev':
                queryOptions.query = 'dev'
                getJobs(iquesta);
                break;
            case 'iquesta-gestion':
                queryOptions.query = 'gestion'
                getJobs(iquesta);
                break;
            case 'iquesta-sys':
                queryOptions.query = 'sys'
                getJobs(iquesta);
                break;
            default:
                getJobs(indeed);
        }
    }
    else {
        res.render('index.ejs',
            {
                jobs: [],
                search: {}
            });
    }
});

/* Debugging purposes */
app.get('/json', (req, res) => {
    let queryOptions = {
        query: req.query.q,
        city: 'Paris',
        radius: '25',
        level: '',
        jobType: 'internship',
        maxAge: '7',
        sort: 'date',
        limit: '10'
    };

    // indeed.query(queryOptions).then(jobsRes => {
    //     res.send(jobsRes);
    // });
    monster.query(queryOptions).then(jobsRes => {
        res.send(jobsRes);
    });
})

app.listen(port, () => console.log("Running!"));
