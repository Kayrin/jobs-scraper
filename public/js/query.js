$("#search-form").submit(function (e) {
    e.preventDefault();

    var url = new URL(window.location.href);
    url.searchParams.set("q", $("#query-form").val());
    url.searchParams.set("loc", $("#location-form").val());
    url.searchParams.set("nb", $("#nb-results option:selected").val());
    url.searchParams.set("site", $("#website-form option:selected").val());

    window.location.href = url.href;

});