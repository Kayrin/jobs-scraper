const request = require('request');
const cheerio = require('cheerio');

module.exports.query = function(queryObject){
  const q = new Query(queryObject);
  return q.getJobs();
};

function Query(qo){
  // query variables
  this.query = '27'; // Informatique - développement
  switch (qo.query) {
    case 'conseil':
      this.query = '26';
      break;
    case 'dev':
      this.query = '27';
      break;
    case 'gestion':
      this.query = '28';
      break;
    case 'sys':
      this.query = '29';
      break;
    default:
      this.query = '27';
  }

  this.city = qo.city || '';
  this.radius = qo.radius || '25';
  this.level = qo.level || '';
  this.maxAge = qo.maxAge || '';
  this.sort = qo.sort || '';
  this.jobType = '1'; // stages

  // internal variables
  this.page = 1;
  this.limit = Number(qo.limit) || 0;
}

Query.prototype.url = function(){
  let q = 'https://www.iquesta.com/offer/result?';
  q += '&l=' + this._cityNameForWeb();
  q += '&contracts=' + this.jobType;
  q += '&disciplines=' + this.query;
  q += '&page=' + this.page;
  console.log(q);
  return q;
}
Query.prototype._cityNameForWeb = function(){
  return this.city.replace(' ', '+').replace(',', '%2C');
};

/* Gets all the desired jobs for the city */
Query.prototype.getJobs = function(){
  return new Promise((resolve, reject) => {
    
    /* Recursive function that gets jobs until it can't anymore (Or shouldn't) */
    function getSomeJobs(self, jobs) {
      request(self.url(), (error, response, body) => {
        const parsed = parseJobList(body);
        jobs = jobs.concat(parsed.jobs);
        if(parsed.error !== null){
          // Got an error so reject
          reject(Error);
        }else if(parsed.continue === true){
          // If we reach the limit stop looping
          if(self.limit != 0 && jobs.length > self.limit){
            while(jobs.length != self.limit) jobs.pop();
            resolve(jobs);
          }else{
            // Continue getting more jobs
            self.page += 1;
            self.startPage += 1;
            getSomeJobs(self, jobs);
          }
        }else{
          // We got all the jobs so stop looping
          resolve(jobs);
        }
      });
    }
    getSomeJobs(this, []);
  });
}

/* Parses a page of jobs */
function parseJobList(body){
  const $ = cheerio.load(body);
  const jobTable = $('.list-offer');
  const jobs = jobTable.find('.offer-summary');
  

  // Create objects
  const jobObjects = jobs.map((i, e) => {
    const job = $(e);

    var location = job.find('.row').children('.col').text();
    var locMatch = location.match(/.*:(.*)$/);
    if (locMatch != null)
      location = locMatch[1].trim();

    return {
      title: job.find('.h5').children('a').text(),
      summary: null,
      url: job.find('.h5').children('a').attr('href'),
      company: job.find('.h6').children('a').text(),
      location: location,
      postDate:  null,
      salary: null
    };
  }).get();

  // check if last page i.e. if the last page-item is the char '>'
  var cont = $(".pagination").children().last().children('a').text() === '>';

  return {
    error: null,
    continue: cont,
    jobs: jobObjects
  };
}