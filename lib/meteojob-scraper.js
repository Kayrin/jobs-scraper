const request = require('request');
const cheerio = require('cheerio');

module.exports.query = function(queryObject) {
    const q = new Query(queryObject);
    return q.getJobs();
};

function Query(qo) {
    // query variables
    this.query = qo.query || '';
    this.city = qo.city || '';
    this.maxAge = qo.maxAge || '';

    // internal variables
    this.page = 1;
    this.limit = Number(qo.limit) || 0;
};

Query.prototype.url = function() {
    let q = 'https://www.meteojob.com/jobsearch/offers?contracts=TRAINING';
    q += '&what=' + encodeURIComponent(this.query);
    q += '&where=' + encodeURIComponent(this.city);
    q += '&_since_=' + this.maxAgeToString(this.maxAge)
    q += '&page=' + this.page;
    console.log(q);
    return q;
};

Query.prototype.maxAgeToString = function(maxAge) {
    let ageString = '';
    if (maxAge >= 1)  ageString = 'day';
    if (maxAge >= 7)  ageString = 'week';
    if (maxAge >= 30) ageString = 'month';
    return ageString;
};

Query.prototype.getJobs = function(){
    return new Promise((resolve, reject) => {

        // Recursive function that gets jobs until it can't anymore (Or shouldn't)
        function getSomeJobs(self, jobs) {
            const options = {
                url: self.url(),
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'
                }
            };

            request(options, (error, response, body) => {
                const parsed = parseJobList(body);
                jobs = jobs.concat(parsed.jobs);

                if (parsed.error !== null) {
                    // Got an error so reject
                    reject(Error);
                } else if (parsed.continue === true) {
                    // If we reach the limit stop looping
                    if (self.limit != 0 && jobs.length > self.limit) {
                        while(jobs.length != self.limit) jobs.pop();
                        resolve(jobs);
                    } else {
                        // Continue getting more jobs
                        self.page += 1;
                        getSomeJobs(self, jobs)
                    }
                } else {
                    // We got all the jobs so stop looping
                    resolve(jobs);
                }
            });
        }

        getSomeJobs(this, []);
    });
};

/* Parses a page of jobs */
function parseJobList(body) {
    const $ = cheerio.load(body);
    const jobTable = $('.mj-offers-list');
    const jobs = jobTable.find('.mj-offer:not(.mail-alert)');

    // No offers were found
    if ($('.mj-block.no-result').length) {
        return {
            error: null,
            continue: false,
            jobs: []
        }
    }

    // Create objects
    const jobObjects = jobs.map((i, e) => {
        const job = $(e);

        // Companies can have an image logo or simple text
        let company = null;
        if (job.find('.logo p').length)
            company = job.find('.logo p').text().trim();
        else
            company = job.find('.logo img').attr('alt').trim();

        // Split infos (type of offer, duration if provided, location)
        const infos = job.find('.info').text().split('•').map((e) => e.trim());
        const duration_regex = infos[0].match(/[0-9]+ mois|[0-9]+ à [0-9]+ mois/gmi);
        const duration = (duration_regex) ? duration_regex[0] : null;
        const location = infos[1];

        return {
            title: job.find('.title').text().trim(),
            summary: job.find('.preview-wrapper').text().trim(),
            url: 'https://www.meteojob.com' + job.find('.block-link').attr('href').trim(),
            company: company,
            location: location,
            postDate: job.find('.published-date').text().trim(),
            duration: duration,
            salary: null
        };
    }).get();

    return {
        error: null,
        continue: $('.pagination-pages').find('li').last().find('a').length !== 0,
        jobs: jobObjects
    };
};
