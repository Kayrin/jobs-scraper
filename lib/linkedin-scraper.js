const request = require('request');
require('javascript-time-ago/load-all-locales');
const TimeAgo = require('javascript-time-ago');

const POST_TIMES = {
    'any_time': '',
    'past_day': '1',
    'past_week': '1%2C2',
    'past_month': '1%2C2%2C3%2C4'
};

const timeAgo = new TimeAgo('fr');

module.exports.query = function(queryObject) {
    const q = new Query(queryObject);
    return q.getJobs();
};

function Query(qo) {
    // query variables
    this.query = qo.query || '';
    this.city = qo.city || 'Paris, France';
    this.radius = qo.radius || '25';
    this.maxAge = this.maxAgeToString(qo.maxAge);

    // internal variables
    this.start = 0;
    this.perPage = 25;
    this.limit = Number(qo.limit) || 0;
};

Query.prototype.url = function() {
    let q = 'https://www.linkedin.com/jobs/searchRefresh?';
    q += '&f_JT=INTERNSHIP'; // job type internship
    q += '&f_I=96%2C6%2C4%2C118'; // IT, Internet, Computer software, Computer & network security
    q += '&f_E=1'; // internship experience level
    q += '&keywords=' + encodeURIComponent(this.query); // query
    q += '&location=' + this.cityNameForWeb(); // location
    q += '&f_TP=' + this.maxAge; // date posted
    q += '&distance=' + this.radius; // distance from location in miles
    q += '&start=' + this.start; // pagination start
    q += '&count=' + this.perPage; // pagination count per page
    console.log(q);
    return q;
};

Query.prototype.cityNameForWeb = function() {
    return this.city.replace(' ', '+').replace(',', '%2C');
};

Query.prototype.maxAgeToString = function(maxAge) {
    let ageString = '';
    if (maxAge >= 1)  ageString = POST_TIMES['past_day'];
    if (maxAge >= 7)  ageString = POST_TIMES['past_week'];
    if (maxAge >= 30) ageString = POST_TIMES['past_month'];
    return ageString;
};

Query.prototype.getJobs = function() {
    return new Promise((resolve, reject) => {

        // Recursive function that gets jobs until it can't anymore
        function getSomeJobs(self, jobs) {
            const options = {
                url: self.url(),
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3'
                }
            };

            // load the page to fake a query
            request(self.url().replace('searchRefresh', 'search'), (error, response, body) => {});

            // retrieve the json page
            request(options, (error, response, body) => {
                if (response.statusCode !== 200) {
                    console.log('Error on LinkedIn (retry).');
                    resolve(jobs);
                }

                const parsed = parseJobList(body, jobs.length, self.limit);
                jobs = jobs.concat(parsed.jobs);

                if (parsed.error !== null) {
                    // Got an error so reject
                    reject(Error);
                } else if (parsed.continue === true) {
                    // If we reach the limit stop looping
                    if (self.limit != 0 && jobs.length > self.limit) {
                        while(jobs.length != self.limit) jobs.pop();
                        resolve(jobs);
                    } else {
                        // Continue getting more jobs
                        self.start += self.perPage;
                        getSomeJobs(self, jobs);
                    }
                } else {
                    // We got all the jobs so stop looping
                    resolve(jobs);
                }
            });
        }

        getSomeJobs(this, []);
    });
};

/* Parses a page of jobs */
function parseJobList(body, jobCount, limit) {
    try {
        var jobsJSON = JSON.parse(body);
    } catch(e) {
        return {
            error: e,
            continue: false,
            jobs: []
        }
    }

    const jobObjects = jobsJSON.decoratedJobPostingsModule.elements.map(j => {
        const job = j.decoratedJobPosting;

        return {
            title: job.jobPosting.title,
            summary: job.formattedDescription,
            url: j.viewJobCanonicalUrl || j.viewJobTextUrl,
            company: job.companyName,
            location: job.jobPostingFormattedCityName || job.formattedLocation || job.cityState,
            postDate: timeAgo.format(job.jobPosting.listDate),
            salary: null
        };
    });

    const hasMoreJobs = jobsJSON.decoratedJobPostingsModule.paging.links.next !== null;
    const currentJobCount = jobCount + jobObjects.length;

    return {
        error: null,
        continue: hasMoreJobs && currentJobCount < limit,
        jobs: jobObjects
    };
};
