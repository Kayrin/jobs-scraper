const request = require('request');
const cheerio = require('cheerio');

module.exports.query = function(queryObject){
  const q = new Query(queryObject);
  return q.getJobs();
};

function Query(qo){
  // query variables
  this.query = qo.query || '';
  this.city = qo.city || '';
  this.radius = qo.radius || '25';
  this.maxAge = qo.maxAge || '';
  this.country = qo.country || 'fr';

  // internal variables
  this.page = 1;
  this.limit = Number(qo.limit) || 0;
}

Query.prototype.url = function(){
  let q = 'https://www.monster.fr/emploi/recherche/Stage-Apprentissage-Alternance_8';
  q += '?q=' + encodeURIComponent(this.query);
  q += '&where=' + this._cityNameForWeb();
  q += '&cy=' + this.country;
  q += '&tm=' + this.maxAge;
  q += '&rad=' + this.radius;
  q += '&stpage=' + ((this.page - 1) <= 1 ? 1 : this.page - 1);
  q += '&page=' + this.page;
  console.log(q);
  return q;
}

Query.prototype._cityNameForWeb = function(){
  return this.city.replace(' ', '-').replace(',', '%2C');
};

/* Gets all the desired jobs for the city */
Query.prototype.getJobs = function(){
  return new Promise((resolve, reject) => {

    /* Recursive function that gets jobs until it can't anymore (Or shouldn't) */
    function getSomeJobs(self, jobs) {
      const options = {
        url: self.url(),
        headers: {
          'User-Agent': 'request'
        }
      };

      request(options, (error, response, body) => {
        if (response.statusCode !== 200) resolve(jobs); // Error 404 means there's no more results

        const parsed = parseJobList(body);
        jobs = parsed.jobs; // No contatenation : Monster sends all results back

        if (parsed.error !== null) {
          // Got an error so reject
          reject(Error);
        } else if (parsed.continue === true) {
          // If we reach the limit stop looping
          if (self.limit != 0 && jobs.length > self.limit) {
            while(jobs.length != self.limit) jobs.pop();
            resolve(jobs);
          } else {
            // Continue getting more jobs
            self.page += 1;
            self.startPage += 1;
            getSomeJobs(self, jobs);
          }
        } else {
          // We got all the jobs so stop looping
          resolve(jobs);
        }
      });
    }
    getSomeJobs(this, []);
  });
}

/* Parses a page of jobs */
function parseJobList(body){
  const $ = cheerio.load(body);
  const jobTable = $('#SearchResults');
  const jobs = jobTable.find('.card-content:not(.featured-ad)');

  // No offers were found
  if ($('header.title').text().includes('aucune offre')) {
    return {
      error: null,
      continue: false,
      jobs: []
    }
  }

  // Filter out ads
  const filtered = jobs.filter((i, e) => {
    const job = $(e);
    return !($.contains(job, 'Offre sponsorisée') || $.contains(job, 'Entreprise à la une'));
  })

  // Create objects
  const jobObjects = filtered.map((i, e) => {
    const job = $(e);

    return {
      title: job.find('.title').children('a').first().text().trim(),
      summary: null,
      url: job.find('.title').children('a').first().attr('href').trim(),
      company: job.find('.company').children('.name').text().trim() || null,
      location: job.find('.location').text().trim(),
      postDate: job.find('time').text().trim(),
      salary: null
    };
  }).get();

  return {
    error: null,
    continue: jobTable.next('#loadMoreJobs').length != 0,
    jobs: jobObjects
  };
}
