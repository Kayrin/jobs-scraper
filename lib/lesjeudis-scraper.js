const request = require('request');
const cheerio = require('cheerio');

module.exports.query = function(queryObject){
  const q = new Query(queryObject);
  return q.getJobs();
};

function Query(qo){
  // query variables
  this.query = qo.query || '';
  this.jobType = 'jtin';
  this.location = qo.city || '';
  this.sort = 'date';

  // internal variables
  this.page = 1;
  this.limit = Number(qo.limit) || 0;
}

Query.prototype.url = function(){
  let q = 'https://www.lesjeudis.com/recherche';
  q += '?q=' + encodeURIComponent(this.query);
  q += '&emp=' + this.jobType;
  q += '&loc=' + this.location;
  q += '&sort=' + this.sort;
  q += '&pg=' + this.page;

  console.log(q);
  return q;
}

/* Gets all the desired jobs for the city */
Query.prototype.getJobs = function(){
  return new Promise((resolve, reject) => {

    /* Recursive function that gets jobs until it can't anymore (Or shouldn't) */
    function getSomeJobs(self, jobs) {
      
      var options = {
        url: self.url(),
        headers: {
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'
        }
      }  

      request(options, (error, response, body) => {
        const parsed = parseJobList(body);
        jobs = jobs.concat(parsed.jobs);

        if (parsed.error !== null) {
          // Got an error so reject
          reject(Error);
        } else if (parsed.continue === true) {
          // If we reach the limit stop looping
          if (self.limit != 0 && jobs.length > self.limit) {
            while(jobs.length != self.limit) jobs.pop();
            resolve(jobs);
          } else {
            // Continue getting more jobs
            self.page += 1;
            self.startPage += 1;
            getSomeJobs(self, jobs);
          }
        } else {
          // We got all the jobs so stop looping
          resolve(jobs);
        }
      });
    }
    getSomeJobs(this, []);
  });
}

/* Parses a page of jobs */
function parseJobList(body){
  const $ = cheerio.load(body);
  const jobTable = $('#jobs-content');
  const jobs = jobTable.find('.job-info');
  
//   const filtered = jobs.filter((i, e) => {
//     const job = $(e);
//     return !(job.find('.simplified-icon').length > 0);
//    });

  // Create objects
  const jobObjects = jobs.map((i, e) => {
    const job = $(e);

    const snapshot = job.find('.snapshot');
    const children =  snapshot.children('.snapshot-item');

    var date = job.find('.date').children('a').text();
    var dateMatch = date.match(/postée (.*)/);
    if (dateMatch != null) {
        date = dateMatch[1].trim();
    }

    return {
      title: job.find('.job-title').text(),
      summary: null,
      url: 'https://www.lesjeudis.com' + job.find('.job-title').attr('href'),
      company: children.eq(0).children('a').text(),
      location: children.eq(1).text(),
      postDate: date,
      salary: null
    };
  }).get();

  var cont = $('#jrp-pagination').children().last().hasClass('btn-arrow');

  return {
    error: null,
    continue: cont,
    jobs: jobObjects
  };
}
