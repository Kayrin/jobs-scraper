const request = require('request');
const cheerio = require('cheerio');

module.exports.query = function(queryObject){
  const q = new Query(queryObject);
  return q.getJobs();
};

// retrieve location ID from a GET request beforehand
module.exports.queryLoc = function(loc) {
  let qLoc = 'https://www.glassdoor.fr/findPopularLocationAjax.htm?maxLocationsToReturn=1';
  qLoc += '&term=' + encodeURIComponent(loc);
  console.log(qLoc);

  var options = {
    url: qLoc,
    headers: {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'
    }
  }

  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (response.statusCode !== 200) {
        console.log(response.statusCode);
        reject(Error);
      }
        var locJSON = JSON.parse(body);
        resolve(locJSON);
    });
  });
}

function Query(qo){
  // query variables
  this.query = qo.query || '';
  this.radius = '30';
  this.maxAge = qo.maxAge || '';
  this.jobType = 'internship';
  this.location = qo.city || '';
  this.locationType = qo.locationType || '';
  this.locationId = qo.locationId || '';

  // internal variables
  this.page = 1;
  this.limit = Number(qo.limit) || 0;
}

Query.prototype.url = function(){
  let q = 'https://www.glassdoor.fr/Job/html/jobs.htm';
  q += '?sc.keyword=' + encodeURIComponent(this.query);
  q += '&jobType=' + this.jobType;
  q += '&fromAge=' + this.maxAge;
  q += '&radius' + this.radius;
  q += '&sc.PageNumber=' + this.page;
  q += '&locKeyword=' + this.location;
  q += '&locT=' + this.locationType;
  q += '&locId=' + this.locationId;

  console.log(q);
  return q;
}

/* Gets all the desired jobs for the city */
Query.prototype.getJobs = function(){
  return new Promise((resolve, reject) => {

    /* Recursive function that gets jobs until it can't anymore (Or shouldn't) */
    function getSomeJobs(self, jobs) {
      
      var options = {
        url: self.url(),
        headers: {
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'
        }
      }  

      request(options, (error, response, body) => {
        const parsed = parseJobList(body);
        jobs = jobs.concat(parsed.jobs);

        if (parsed.error !== null) {
          // Got an error so reject
          reject(Error);
        } else if (parsed.continue === true) {
          // If we reach the limit stop looping
          if (self.limit != 0 && jobs.length > self.limit) {
            while(jobs.length != self.limit) jobs.pop();
            resolve(jobs);
          } else {
            // Continue getting more jobs
            self.page += 1;
            self.startPage += 1;
            getSomeJobs(self, jobs);
          }
        } else {
          // We got all the jobs so stop looping
          resolve(jobs);
        }
      });
    }
    getSomeJobs(this, []);
  });
}

/* Parses a page of jobs */
function parseJobList(body){
  const $ = cheerio.load(body);
  const jobTable = $('.jlGrid');
  const jobs = jobTable.find('.jl');

  // Filter out ads
  // const filtered = jobs.filter((i, e) => {
  //   const job = $(e);
  //   return !($.hasClass(job, 'HotListing'));
  // })

  // Create objects
  const jobObjects = jobs.map((i, e) => {
    const job = $(e);

    var company = job.find('.empLoc').children('div').first().text();
    var companyMatch = company.match(/(.*) –/);
    if (companyMatch != null)
      company = companyMatch[1].trim();

    return {
      title: job.find('.jobLink').text(),
      summary: null,
      url: 'https://www.glassdoor.fr/partner/jobListing.htm?jobListingId=' + job.attr('data-id'),
      company: company,
      location: job.attr('data-job-loc'),
      postDate: job.find('.empLoc').find('.minor').text(),
      salary: null
    };
  }).get();

  var cont = $('#ResultsFooter').find('.next').children('a').length > 0;

  return {
    error: null,
    continue: cont,
    jobs: jobObjects
  };
}
